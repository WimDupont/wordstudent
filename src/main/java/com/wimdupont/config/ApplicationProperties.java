package com.wimdupont.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.MissingResourceException;
import java.util.Properties;

public class ApplicationProperties {

    private static ApplicationProperties instance;
    private final String couchdbUrl;
    private final String dictionaryClientUrl;
    private final String wordCsvDir;

    public String getDictionaryClientUrl() {
        return dictionaryClientUrl;
    }

    public String getCouchdbUrl() {
        return couchdbUrl;
    }

    public String getWordCsvDir() {
        return wordCsvDir;
    }

    private ApplicationProperties() {
        Properties properties = loadProperties();
        couchdbUrl = properties.getProperty("couchdb.connection-url");
        dictionaryClientUrl = properties.getProperty("dictionary.client.connection-url");
        wordCsvDir = properties.getProperty("csv.dir");
        if (couchdbUrl == null | dictionaryClientUrl == null | wordCsvDir == null)
            throw new RuntimeException("Properties missing.");
    }

    public static ApplicationProperties getInstance() {
        if (instance == null) {
            instance = new ApplicationProperties();
        }

        return instance;
    }

    private Properties loadProperties() {
        final Properties properties = new Properties();
        InputStream inputStream = getClass().getResourceAsStream("/application.properties");
        try {
            if (inputStream == null) {
                throw new IOException();
            }
            properties.load(inputStream);
        } catch (IOException e) {
            throw new MissingResourceException("Missing application properties", Properties.class.getSimpleName(), "application.properties");
        }
        return properties;
    }
}
