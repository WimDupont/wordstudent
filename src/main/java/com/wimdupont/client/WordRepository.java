package com.wimdupont.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wimdupont.config.ApplicationProperties;
import com.wimdupont.model.db.WordCouchDocument;
import com.wimdupont.model.db.WordSelectorResponse;
import com.wimdupont.model.dto.WordDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class WordRepository {

    private final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    public List<String> findAllWords() {
        return Client.get(applicationProperties.getCouchdbUrl() + "/_all_docs?include_docs=true", WordCouchDocument.class)
                .map(wordCouchDocument -> wordCouchDocument.rows().stream().map(row -> row.doc().word()).toList())
                .orElseGet(ArrayList::new);
    }

    public Optional<WordDto> findByWord(String word) {
        var url = applicationProperties.getCouchdbUrl() + "/_find";
        var requestString = String.format("{ \"selector\": { \"word\": { \"$eq\": \"%s\" } } }", word);
        return Client.post(url, WordSelectorResponse.class, requestString).flatMap(f -> f.docs().stream().findAny());
    }

    public void save(WordDto wordDto) {
        if (findByWord(wordDto.word()).isEmpty()) {
            var url = applicationProperties.getCouchdbUrl();
            try {
                Client.post(url, WordSelectorResponse.class, new ObjectMapper().writeValueAsString(wordDto));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.printf("Word %s already saved", wordDto.word());
        }
    }
}
