package com.wimdupont.service;

import com.wimdupont.config.ApplicationProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordFetcher {

    private final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    public List<String> fetch() {
        List<String> words = new ArrayList<>();
        try (var lines = Files.lines(Path.of(applicationProperties.getWordCsvDir()))) {
            lines.forEach(line -> words.addAll(Arrays.stream(line.split(";"))
                    .map(String::trim).toList()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return words;
    }
}
