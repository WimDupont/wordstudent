package com.wimdupont.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record WordDto (
        String word,
        List<DictionaryDto> dictionaryResults
){
}
