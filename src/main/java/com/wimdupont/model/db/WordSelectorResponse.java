package com.wimdupont.model.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wimdupont.model.dto.WordDto;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record WordSelectorResponse(
        List<WordDto> docs

) {
}
